<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\exception
{
	use nuclio\exception\NuclioException;
	
	class DatabaseException extends NuclioException
	{
		private Map<arraykey,mixed> $debug=Map{};
		
		public function setDebug(Map<arraykey,mixed> $debug):void
		{
			$this->debug=$debug;
		}
		
		public function getDebug():Map<mixed,mixed>
		{
			return $this->debug;
		}
		
		public function getMessage():string
		{
			$message=parent::getMessage();
			$message.='[DEBUG] '.print_r($this->debug,true);
			return $message;
		}
	}
}
